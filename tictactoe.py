
def print_field():
    global field
    print("---------")
    for i in range(3):
        print("| ", end="")
        for j in range(3):
            print(field[i][j], end=" ")
        if i == 2:
            break
        print("|")
    print("|\n---------")


def swap_player():
    global player
    if player == "X":
        player = "O"
    else:
        player = "X"


def make_move():
    global field
    while True:
        coords = input("Enter the coordinates: ").split()
        if not coords[0].isnumeric() or not coords[1].isnumeric():
            print("You should enter numbers!")
        elif int(coords[0]) not in range(1, 4) or int(coords[1]) not in range(1, 4):
            print("Coordinates should be from 1 to 3!")
        elif field[3 - int(coords[1])][int(coords[0]) - 1] != "_":
            print("This cell is occupied! Choose another one!")
        else:
            field[3 - int(coords[1])][int(coords[0]) - 1] = player
            break


def check_win():
    for i in range(3):
        if field[i] == win_cond_x or field[i] == win_cond_o:
            return True
        row = ["" for k in range(3)]
        for j in range(3):
            row[j] = field[j][i]
        if row == win_cond_x or row == win_cond_o:
            return True
    else:
        row = ["" for k in range(3)]
        for i in range(3):
            row[i] = field[i][i]
        if row == win_cond_x or row == win_cond_o:
            return True
        for i in range(3):
            row[i] = field[i][2 - i]
        if row == win_cond_x or row == win_cond_o:
            return True
    return False


field = [["_" for i in range(3)] for j in range(3)]
print_field()
player = "X"
win_cond_x = ["X", "X", "X"]
win_cond_o = ["O", "O", "O"]
for i in range(9):
    make_move()
    print_field()
    if check_win():
        print(f"{player} wins")
        break
    swap_player()
else:
    print("Draw")
